import serial
from PySide2.QtWidgets import QMainWindow, QPushButton
from PySide2.QtUiTools import QUiLoader
from PySide2.QtCore import Qt, QObject, QThread, Signal, QFile

from inventory.dialogs.asset_form import AssetForm

class ScanWorker(QThread):
    signal = Signal(str)

    def __init__(self):
        super(ScanWorker, self).__init__()
        
    def run(self):
        serial_port = serial.Serial('/COM5')
        while self.isRunning():
            asset = serial_port.readline()
            asset = asset.decode('utf-8').replace('\r\n', '')
            self.signal.emit(asset)

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        
        self.setWindowTitle('Scan Asset')
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.window = None
        self.asset_form = AssetForm()

        ui_file = QFile('inventory/ui/mainwindow.ui')
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()
        self.window.show()

    def listen_for_asset(self):
        self.worker = ScanWorker()
        self.worker.signal.connect(self.setup_asset_form)
        self.worker.start()

    def setup_asset_form(self, sigstr):
        self.asset_form.update(sigstr)



