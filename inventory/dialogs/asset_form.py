from PySide2.QtWidgets import QDialog, QLineEdit
from PySide2.QtUiTools import QUiLoader
from PySide2.QtCore import QFile

class AssetForm(QDialog):
   def __init__(self, asset_tag='', parent=None):
      super(AssetForm, self).__init__(parent)

      self.setWindowTitle('Edit or add new asset')
      ui_file = QFile('inventory/ui/asset.ui')
      ui_file.open(QFile.ReadOnly)
      loader = QUiLoader()
      self.dialog = loader.load(ui_file, self)
      self.dialog.setWindowTitle('Asset')
      asset_edit = self.dialog.findChild(QLineEdit, 'asset')
      asset_edit.setEnabled(False)
      asset_edit.setText(asset_tag)
      ui_file.close()

   def update(self, asset_tag):
      asset_edit = self.dialog.findChild(QLineEdit, 'asset')
      asset_edit.setText(asset_tag)
      self.dialog.show()