from PySide2.QtWidgets import QApplication, QLabel
from inventory.dialogs.main_window import MainWindow

if __name__ == '__main__':
    app = QApplication()
    window = MainWindow()
    window.listen_for_asset()
    app.exec_()